type document = unit;
[@bs.val] external document: document = "document";
[@bs.send] external getElementById: (document, string) => Dom.element = "getElementById";
ReactDOMRe.render(<Piano />, getElementById(document, "root"));
