[@react.component]
let make: unit => React.element;

/* including this quiets "unused module" errors */
type model;
module Debug {
    [@react.component]
    let make: (~model: model)=> React.element;
}