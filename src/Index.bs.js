'use strict';

var React = require("react");
var ReactDom = require("react-dom");
var Piano$PianoV2 = require("./Piano.bs.js");

ReactDom.render(React.createElement(Piano$PianoV2.make, {}), document.getElementById("root"));

/*  Not a pure module */
