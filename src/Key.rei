type playable = {name: string, keycode: int, keydoc: string, frequency: float};

type t =
  | Playable(playable)
  | Spacer;

let white: array(t);
let black: array(array(t));
let byKeyCode: Belt.Map.Int.t(playable);