/* type playing = {oscillator: Audio.oscillator, gain: Audio.gain} */
type playing = (Audio.oscillator, unit);

type model = {
  audioContext: option(Audio.context),
  wave: Belt.Result.t(Audio.wave, string),
  pressed: Belt.Set.String.t,
  playing: Belt.Map.String.t(playing),
};
let init: model = {
  audioContext: None,
  wave: Belt.Result.Error("loading"),
  pressed: Belt.Set.String.empty,
  playing: Belt.Map.String.empty,
};

type msg =
  | ClickedNoteOn(Key.playable)
  | ClickedNoteOff(Key.playable)
  | NoteStarted(Key.playable, playing)
  | NoteStopped(Key.playable, playing)
  | InitAudioContext(option(Audio.context))
  | InitWavetable(Belt.Result.t(Audio.wave, string));

module KeyButton {
  [@react.component]
  let make = (~key_: Key.t, ~model: model, ~dispatch: msg => unit) => {
    switch(key_) {
    | Key.Spacer => <button></button>
    | Key.Playable(key) =>
      <button
        title={key.name}
        className={if (Belt.Set.String.has(model.pressed, key.name)) "pressed" else ""}
        onMouseDown={_ => dispatch(ClickedNoteOn(key))}
        onMouseUp={_ => dispatch(ClickedNoteOff(key))}
        onMouseOut={_ => dispatch(ClickedNoteOff(key))}
        onTouchStart={_ => dispatch(ClickedNoteOn(key))}
        onTouchEnd={_ => dispatch(ClickedNoteOff(key))}
      >
        {React.string(key.keydoc)}
      </button>
    }
  };
}

module Debug {
  [@react.component]
  let make = (~model: model) => {
    <>
      <div>
        {React.string(switch(model.audioContext) {
        | Some(audio) => "audio enabled :) " ++ audio.state
        | None => "audio disabled :("
        })}
      </div>
      <div>
        {React.string(switch(model.wave) {
        | Belt.Result.Ok(_) => "piano enabled :)"
        | Belt.Result.Error(err) => "piano broken: " ++ err
        })}
      </div>
      <div>
        {React.string("pressed: ")}
        {model.pressed -> Belt.Set.String.toList |> String.concat(",") |> React.string}
      </div>
    </>
  }
}

module UI {
  [@react.component]
  let make = (~model: model, ~dispatch: msg => unit) => {
    <div className="piano">
      <ul className="white-keys">{
        Key.white
        -> Belt.Array.mapWithIndex((i, key_) =>
          <li key={Belt.Int.toString(i)}>
            <KeyButton key_={key_} model={model} dispatch={dispatch} />
          </li>
        )
        -> React.array
      }</ul>
      <ul className="black-keys">{
        Key.black
        -> Belt.Array.mapWithIndex((j, group) => {
          <li key={Belt.Int.toString(j)}><ul className="black-key-group">{
            group -> Belt.Array.mapWithIndex((i, key_) =>
              <li key={Belt.Int.toString(i)}>
                <KeyButton key_={key_} model={model} dispatch={dispatch} />
              </li>
            )
            -> React.array
          }</ul></li>
        })
        -> React.array
      }</ul>
    </div>;
  }
}

module Cmd {
  type t =
    | NoteOn(Key.playable)
    | NoteOff(Key.playable)
    | InitAudio;

  let run = (dispatch: msg => unit, model: model, cmd: t): unit => {
    switch((model.audioContext, cmd)) {
      | (_, InitAudio) => {
        let maybeAudio = Audio.createContext()
        let _ = Belt.Option.map(maybeAudio, audio => Js.Promise.(
          /* from https://github.com/GoogleChromeLabs/web-audio-samples/tree/gh-pages/samples/audio/wave-tables/Piano */
          Fetch.fetch("/src/piano-wavetable.json")
          /* Fetch.fetch("/src/organ-2-wavetable.json") */
          |> then_(Fetch.Response.json)
          |> then_(json => json -> Audio.wavetableDecoder -> resolve)
          |> then_((json: Audio.wavetable) => {
            let _ = 
              Audio.createPeriodicWave(audio, json.real, json.imag)
              -> Belt.Result.Ok
              -> InitWavetable
              -> dispatch
            resolve(())
          })
          |> catch(err => Js.log(err) -> resolve)
        ))
        dispatch(InitAudioContext(maybeAudio))
      }
      | (None, _) => ()
      | (Some(audio), NoteOn(key)) => {
        switch(model.playing -> Belt.Map.String.get(key.name)) {
          | Some(_) => {
            /* keep playing if it's already playing */
            ()
          }
          | None => {
            /* start playing if not yet playing */
            /* based on https://stackoverflow.com/a/29641185/2782048 */
            let osc = Audio.createOscillator(audio)
            /* let gain = Audio.createGain(audio) */
            let gain = ()
            Audio.connectOscillatorDestination(osc, audio.destination)
            /* Audio.connectOscillatorGain(osc, gain) */
            /* Audio.connectGainDestination(gain, audio.destination) */
            let _ = model.wave -> Belt.Result.map(wave => Audio.setPeriodicWave(osc, wave))
            Audio.setFrequency(osc.frequency, key.frequency)
            Audio.startNow(osc)
            dispatch(NoteStarted(key, (osc, gain)))
          }
        }
      }
      | (Some(_), NoteOff(key)) => {
        switch(model.playing -> Belt.Map.String.get(key.name)) {
          | None => ()
          | Some((osc, gain)) => {
            Audio.stopNow(osc)
            dispatch(NoteStopped(key, (osc, gain)))
          }
        }
      }
    }
  }
}

let initKeyboardEvents = dispatch => {
  let keydown = e => {
    let keycode: int = ReactEvent.Keyboard.keyCode(e)
    let key: option(Key.playable) = Belt.Map.Int.get(Key.byKeyCode, keycode)
    Belt.Option.mapWithDefault(key, (), k => dispatch(ClickedNoteOn(k)))
    /* Js.log(("keydown", keycode, key, e)) */
  }
  let keyup = e => {
    let keycode: int = ReactEvent.Keyboard.keyCode(e)
    let key: option(Key.playable) = Belt.Map.Int.get(Key.byKeyCode, keycode)
    Belt.Option.mapWithDefault(key, (), k => dispatch(ClickedNoteOff(k)))
    /* Js.log(("keyup", keycode, e)) */
  }
  Audio.addKeyboardEventListener("keydown", keydown)
  Audio.addKeyboardEventListener("keyup", keyup)
  Some(() => {
    Audio.removeKeyboardEventListener("keydown", keydown)
    Audio.removeKeyboardEventListener("keyup", keyup)
  })
};

let update: (model, msg) => (model, list(Cmd.t)) = (model, msg) => {
  switch(msg) {
  | InitAudioContext(audioContext) => ({...model, audioContext: audioContext}, [])
  | InitWavetable(wave) => ({...model, wave: wave}, [])
  | ClickedNoteOn(key) => ({...model, pressed: model.pressed -> Belt.Set.String.add(key.name)}, [Cmd.NoteOn(key)])
  | ClickedNoteOff(key) => ({...model, pressed: model.pressed -> Belt.Set.String.remove(key.name)}, [Cmd.NoteOff(key)])
  | NoteStarted(key, playing) => ({...model, playing: model.playing -> Belt.Map.String.set(key.name, playing)}, [])
  | NoteStopped(key, _) => ({...model, playing: model.playing -> Belt.Map.String.remove(key.name)}, [])
  }
};
let reducer: ((model, list(Cmd.t)), msg) => (model, list(Cmd.t)) = ((model, _), msg) => update(model, msg);

[@react.component]
let make = () => {
  let ((model, cmds), dispatch) = React.useReducer(reducer, (init, [Cmd.InitAudio]));
  React.useEffect2(() => {
    let _ = cmds |> List.map(cmd => Cmd.run(dispatch, model, cmd))
    None
  }, (model, cmds));
  /* subscriptions require cleanup, so they don't really fit the cmd model (even in elm!) */
  React.useEffect0(() => initKeyboardEvents(dispatch));

  <>
    /* <Debug model={model} /> */
    <UI model={model} dispatch={dispatch} />
  </>
};