type destination = unit;
type timestamp = float;
type context = {destination: destination, currentTime: timestamp, state: string};
type frequency = {value: float};
type oscillator = {frequency: frequency};
type gain = unit;
type wave = unit;

[@bs.val] external _hasWindowAudioContext: bool = "window.audioContext";
[@bs.new] external _windowAudioContext: unit => context = "window.audioContext";
[@bs.val] external _hasWindowAudioContextCaps: bool = "window.AudioContext";
[@bs.new] external _windowAudioContextCaps: unit => context = "window.AudioContext";
[@bs.val] external _hasWindowWebkitAudioContext: bool = "window.webkitAudioContext";
[@bs.new] external _windowWebkitAudioContext: unit => context = "window.webkitAudioContext";

[@bs.send] external createOscillator: context => oscillator = "createOscillator";
[@bs.send] external createGain: context => gain = "createGain";

[@bs.set] external setFrequency: (frequency, float) => unit = "value";

[@bs.send] external connectOscillatorGain: (oscillator, gain) => unit = "connect";
[@bs.send] external connectOscillatorDestination: (oscillator, destination) => unit = "connect";
[@bs.send] external connectGainDestination: (gain, destination) => unit = "connect";
[@bs.send] external start: (oscillator, timestamp) => unit = "start";
[@bs.send] external startNow: (oscillator) => unit = "start";
[@bs.send] external stop: (oscillator, timestamp) => unit = "stop";
[@bs.send] external stopNow: (oscillator) => unit = "stop";

[@bs.send] external createPeriodicWave: (context, Js.Json.t, Js.Json.t) => wave = "createPeriodicWave";
[@bs.send] external setPeriodicWave: (oscillator, wave) => unit= "setPeriodicWave";

/* https://stackoverflow.com/questions/51072468/how-to-handle-global-dom-events-in-reasonml-reasonreact */
[@bs.val] external addKeyboardEventListener: (string, ReactEvent.Keyboard.t => unit) => unit = "addEventListener"
[@bs.val] external removeKeyboardEventListener: (string, ReactEvent.Keyboard.t => unit) => unit = "removeEventListener"

let createContext: unit => option(context) = () => {
  /* [_windowAudioContext, _windowAudioContextCaps, _windowWebkitAudioContext] */
  [
    (_hasWindowAudioContext, _windowAudioContext),
    (_hasWindowAudioContextCaps, _windowAudioContextCaps),
    (_hasWindowWebkitAudioContext, _windowWebkitAudioContext),
  ]
    -> Belt.List.keep(((has, _)) => has)
    -> Belt.List.map(((_, fn)) => fn)
    -> Belt.List.head
    -> Belt.Option.map(f => f())
}

type wavetable = {real: Js.Json.t, imag: Js.Json.t};
let wavetableDecoder = (json: Js.Json.t): wavetable => Json.Decode.{
  real: json |> field("real", id),
  imag: json |> field("imag", id),
};