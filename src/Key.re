/* unfortunate that this and `t` are duplicated, but apparently that's how it's done in ocaml */
type playable = {name: string, keycode: int, keydoc: string, frequency: float};

type t =
  | Playable(playable)
  | Spacer;

let playable = (~keycode: option(int)=?, name: string, keydoc: string, octave: int, step: int): t => Playable({
  name: name,
  keydoc: keydoc,
  keycode: switch(keycode) {
  | Some(k) => k
  | None => keydoc |> Js.String.charCodeAt(0) |> Belt.Int.fromFloat
  },
  /* Manually calculate note frequency (pitch). */
  /* https://en.wikipedia.org/wiki/Equal_temperament#Mathematics */
  /* let interval: float = 2.0 ** (1.0 /. 12.0); */
  frequency: 440.0 *. (2.0 ** (Belt.Int.toFloat(octave - 4) +. (Belt.Int.toFloat(step) /. 12.0))),
});

let white = [|
  playable("C3", "Z", 3, 3),
  playable("D3", "X", 3, 5),
  playable("E3", "C", 3, 7),
  playable("F3", "V", 3, 8),
  playable("G3", "B", 3, 10),
  playable("A4", "N", 4, 0),
  playable("B4", "M", 4, 2),
  /* --- */
  playable("C4", "Q", 4, 3),
  playable("D4", "W", 4, 5),
  playable("E4", "E", 4, 7),
  playable("F4", "R", 4, 8),
  playable("G4", "T", 4, 10),
  playable("A5", "Y", 5, 0),
  playable("B5", "U", 5, 2),
  /* --- */
  playable("C5", "I", 5, 3),
  playable("D5", "O", 5, 5),
  playable("E5", "P", 5, 7),
  playable("F5", "[", 5, 8, ~keycode=219),
  playable("G5", "]", 5, 10, ~keycode=221),
  playable("A6", "\\", 6, 0, ~keycode=220),
  Spacer,
|];

let black = [|
  [|
    playable("C#3", "S", 3, 4),
    playable("D#3", "D", 3, 6),
  |],[|
    playable("F#3", "G", 3, 9),
    playable("G#3", "H", 3, 11),
    playable("A#4", "J", 4, 1),
  |],[|
    playable("C#4", "2", 4, 4),
    playable("D#4", "3", 4, 6),
  |],[|
    playable("F#4", "5", 4, 9),
    playable("G#4", "6", 4, 11),
    playable("A#5", "7", 5, 1),
  |],[|
    playable("C#5", "9", 5, 4),
    playable("D#5", "0", 5, 6),
  |],[|
    playable("F#5", "=", 5, 9, ~keycode=187),
    /* "quotes" break for unicode, see https://github.com/reasonml/reason-react/issues/213 */
    playable("G#5", {js|←|js}, 5, 11, ~keycode=8),
    Spacer,
  |],
|];

let byKeyCode =
  Array.append(white, Belt.Array.concatMany(black))
  -> Belt.Array.keepMap(key => switch(key) {
    | Spacer => None
    | Playable(p) => Some((p.keycode, p))
  })
  -> Belt.Map.Int.fromArray;